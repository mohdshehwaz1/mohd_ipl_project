const csv = require('csvtojson')
const path =require('path')
const fs = require('fs')

const deliveriesFile= "./data/deliveries.csv"
const matchesFile = "./data/matches.csv"



function convertcsvToJSON(csvFilePath,jsonFilePath) {
    const absCSVFilePath =path.join(__dirname,csvFilePath)
    
    

    csv() 
.fromFile(csvFilePath)
.then((jsonObj) => {
    const JSONFilePath =renameFilePath(absCSVFilePath)
    

    writeToJSON(JSONFilePath,jsonObj)     
    })
}


function renameFilePath(filepath) {
    return filepath.replace('.csv','.json');
}


function writeToJSON(JSONFilePath,data) {
    fs.writeFile(JSONFilePath,JSON.stringify(data),function (err,data) {
        if(err) {
            console.error(err);
        }
    })
}


convertcsvToJSON(deliveriesFile)
convertcsvToJSON(matchesFile)