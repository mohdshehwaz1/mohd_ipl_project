
const path = require('path')
const fs = require('fs')

const ipl = require('./ipl.js')


const deliveriesData = require(path.join(__dirname,'../data/deliveries.json'))
const matchesData = require(path.join(__dirname,'../data/matches.json'))

//Code for problem1 

const MatchesPerYear=ipl.numberOfMatches(matchesData,deliveriesData);

const saveData = (data,file) =>{
    const finished = (error) => {
        if(error) {
            console.error(error)
            return;
        }

    }
    const jsonData = JSON.stringify(data)
    fs.writeFile('./src/public/output/MatchesPerYear.json',jsonData,finished)

}

saveData(MatchesPerYear)


//Code for Problem2

const matchesWonByPerTeamInYear=ipl.matchesWonByPerTeamPerYear(matchesData,deliveriesData);

const Problem2 = (data,file) =>{
    const finished = (error) => {
        if(error) {
            console.error(error)
            return;
        }

    }
    const jsonData = JSON.stringify(data)
    fs.writeFile('./src/public/output/MatchesWonByTeam.json',jsonData,finished)

}

Problem2(matchesWonByPerTeamInYear)



//code for problem3

const extrarunsByTeams=ipl.extraRunsByEachteam(matchesData,deliveriesData);

const Problem3 = (data,file) =>{
    const finished = (error) => {
        if(error) {
            console.error(error)
            return;
        }

    }
    const jsonData = JSON.stringify(data)
    fs.writeFile('./src/public/output/ExtraRunsByTeams.json',jsonData,finished)

}

Problem3(extrarunsByTeams)



//code for problem4

const topEconomicBowler=ipl.topTenBowlers(matchesData,deliveriesData);

const Problem4 = (data,file) =>{
    const finished = (error) => {
        if(error) {
            console.error(error)
            return;
        }

    }
    const jsonData = JSON.stringify(data)
    fs.writeFile('./src/public/output/TopEconomicBowler.json',jsonData,finished)

}

Problem4(topEconomicBowler)


// code for extra problem1
 const teamwinner = ipl.extraProblem1(matchesData,deliveriesData)
 const extraProblem1 = (data,file) =>{
    const finished = (error) => {
        if(error) {
            console.error(error)
            return;
        }

    }
    const jsonData = JSON.stringify(data)
    fs.writeFile('./src/public/output/TeamWinner.json',jsonData,finished)

}

extraProblem1(teamwinner)


// code for extra problem2
const HighestManOfTheMatch = ipl.extraProblem2(matchesData,deliveriesData)
const extraProblem2 = (data,file) =>{
   const finished = (error) => {
       if(error) {
           console.error(error)
           return;
       }

   }
   const jsonData = JSON.stringify(data)
   fs.writeFile('./src/public/output/HighestManOfTheMatch.json',jsonData,finished)

}

extraProblem2(HighestManOfTheMatch)


// code for extra problem3
const strikeRate = ipl.extraProblem3(matchesData,deliveriesData)
const extraProblem3 = (data,file) =>{
   const finished = (error) => {
       if(error) {
           console.error(error)
           return;
       }

   }
   const jsonData = JSON.stringify(data)
   fs.writeFile('./src/public/output/StrikeRate.json',jsonData,finished)

}

extraProblem3(strikeRate)



// code for extra problem4
const highestNumberOfDismiss= ipl.extraProblem4(matchesData,deliveriesData)
const extraProblem4 = (data,file) =>{
   const finished = (error) => {
       if(error) {
           console.error(error)
           return;
       }

   }
   const jsonData = JSON.stringify(data)
   fs.writeFile('./src/public/output/HighestNumberOfDismissal.json',jsonData,finished)

}

extraProblem4(highestNumberOfDismiss)


// code for extra problem5
const bestEconomicalBowler= ipl.extraProblem5(matchesData,deliveriesData)
const extraProblem5 = (data,file) =>{
   const finished = (error) => {
       if(error) {
           console.error(error)
           return;
       }

   }
   const jsonData = JSON.stringify(data)
   fs.writeFile('./src/public/output/BestEconomicalBowler.json',jsonData,finished)

}

extraProblem5(bestEconomicalBowler)








