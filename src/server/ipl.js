
//code for problem1

function numberOfMatches(matchesData,deliveriesData) {
    
    let numberOfMatchesPerYear = {}
    for(i=0;i<matchesData.length;i++) {
        if(matchesData[i]['season'] in numberOfMatchesPerYear) {
            numberOfMatchesPerYear[matchesData[i]['season']]=numberOfMatchesPerYear[matchesData[i]['season']]+1
            
        }
        else{
            numberOfMatchesPerYear[matchesData[i]['season']]=1
        }
    }
    return numberOfMatchesPerYear

}

//code for problem2

function matchesWonByPerTeamPerYear(matchesData,deliveriesData) {
    let teamwinner = {}
    for(i=0;i<matchesData.length;i++) {
        if(matchesData[i]['winner'] in teamwinner) {
            teamData={}
            teamData=teamwinner[matchesData[i]['winner']]
            if(matchesData[i]['season'] in teamData) {
                teamData[matchesData[i]['season']]=teamData[matchesData[i]['season']] +1
            }
            else{
                teamData[matchesData[i]['season']]=1
            }

        }
        else {
            teamData={}
            teamData[matchesData[i]['season']]=1
            teamwinner[matchesData[i]['winner']]=teamData

        }
    }
    
    return teamwinner
    


}

//code for problem3

function extraRunsByEachteam(matchesData,deliveriesData) {
    extraRuns={}
    for(i=0;i<matchesData.length;i++) {
        if(matchesData[i]['season']==2016) {
            matchId=matchesData[i]['id']
            for(j=0;j<deliveriesData.length;j++) {
                if(deliveriesData[j]['match_id']==matchId) {
                    if(deliveriesData[j]['bowling_team'] in extraRuns) {
                        let a=deliveriesData[j]['extra_runs']
                        let b=parseInt(a)
                        extraRuns[deliveriesData[j]['bowling_team']]=extraRuns[deliveriesData[j]['bowling_team']] + b
                    }
                    else {
                        let a=deliveriesData[j]['extra_runs']
                        let b=parseInt(a)
                        extraRuns[deliveriesData[j]['bowling_team']]=b
                        
                    }

                }
            }

        }
    }
    return extraRuns
    
}

//code for problem4


function topTenBowlers(matchesData,deliveriesData) {
    topBowlers={}
    bowlerMatch={}
    for(i=0;i<matchesData.length;i++) {
        if(matchesData[i]['season']==2016) {
            matchId=matchesData[i]['id']
            for(j=0;j<deliveriesData.length;j++) {
                if(deliveriesData[j]['match_id']==matchId) {
                    if(deliveriesData[j]['bowler'] in topBowlers) {
                        let a=deliveriesData[j]['total_runs']
                        let b=parseInt(a)
                        topBowlers[deliveriesData[j]['bowler']]=topBowlers[deliveriesData[j]['bowler']] + b
                    }
                    else {
                        let a=deliveriesData[j]['total_runs']
                        let b=parseInt(a)
                        topBowlers[deliveriesData[j]['bowler']]=b
                        
                    }
                    if(deliveriesData[i]['bowler'] in bowlerMatch) {
                        bowlerMatch[deliveriesData[i]['bowler']]=bowlerMatch[deliveriesData[i]['bowler']]+1
                    }
                    else {
                        bowlerMatch[deliveriesData[i]['bowler']]=1
                    }

                }
            }

        }
    }
    const topTen = Object.keys(topBowlers).sort(function(a,b){return topBowlers[a]-topBowlers[b]}).slice(0,10)
    
    return topTen 

}

//code for extra problem1

function extraProblem1(matchesdata,deliveriesdata) {

    const winningTeamByToss=matchesdata.reduce((winningTeam,i) => {
        if(i.toss_winner == i.winner) {
            if(i.winner in winningTeam) {
                winningTeam[i.winner]=winningTeam[i.winner] +1
            }
            else {
                winningTeam[i.winner]=1
            }
        }
        return winningTeam;
        
    },{})
    return winningTeamByToss

    

}

//code for extra problem2


function extraProblem2(matchesdata,deliveriesdata) {
    
    const manOfTheMatch = matchesdata.reduce((highestManOfTheMatch,i) =>{
        const manOfTheMatch=i.player_of_match
        const season=i.season
        if(manOfTheMatch !=undefined && season !=undefined) {
            if(highestManOfTheMatch[season] ==undefined || highestManOfTheMatch[season]==null) {
                highestManOfTheMatch[season]={}
            }
            if(highestManOfTheMatch[season][manOfTheMatch]==undefined || highestManOfTheMatch[season][manOfTheMatch]==null) {
                highestManOfTheMatch[season][manOfTheMatch]=0
            }

            highestManOfTheMatch[season][manOfTheMatch] +=1
        }
        return highestManOfTheMatch;
        
    },{})
    const result = Object.values(manOfTheMatch)
    const finalresult={}
    for(i=0;i<result.length;i++) {
        const another =Object.entries(result[i]).sort((prev, next) => prev[1] - next[1])
        let final={}
        final=another.pop()
       
        finalresult[Object.keys(manOfTheMatch)[i]]=final

        
    }
    return finalresult
    
    


}

//code for extra problem3



function extraProblem3(matchesdata,deliveriesdata) {

    const matchesIdandyear = matchesdata.reduce((matchesIdandSeason,index) => {
        matchesIdandSeason[index.id]=index.season
        
        return matchesIdandSeason

    },{})
   
    const years=[]
    const storeRunsandBallsByDhoni = deliveriesdata.reduce((storeRunsandBalls,index) => {

        const seasons=matchesIdandyear[index.match_id]
        
        if(years==undefined){
            years.push(seasons)
        }
        else{
            if(seasons!=years[years.length -1]){
                years.push(seasons)
            }
        }
        
       
        if(index.batsman == 'MS Dhoni') {
            if(storeRunsandBalls[index.batsman]==undefined) {
                storeRunsandBalls[index.batsman]={}
            }
            if(storeRunsandBalls[index.batsman][seasons]==undefined) {
                storeRunsandBalls[index.batsman][seasons]={}
                

            }
            
            storeRunsandBalls[index.batsman][seasons].bowls=(storeRunsandBalls[index.batsman][seasons].bowls || 0) +1
            storeRunsandBalls[index.batsman][seasons].runs=(storeRunsandBalls[index.batsman][seasons].runs || 0) +Number(index.batsman_runs)


        }
        return storeRunsandBalls

    },{})
   
    const finalresult={}
    const player='MS Dhoni'
    const allYears=Object.values(storeRunsandBallsByDhoni)
    
    
   
    for(i=0;i<10;i++) {
        year=years[i]
        if(player in finalresult) {
            if(year in finalresult[player]) {


            }
            else{
                let strike={}
                strike['strike-Rate']=Number((Number(allYears[0][year]['runs'])/Number(allYears[0][year]['bowls']))*100)
                
                finalresult[player][year]=strike

            }

        }
        else {
            let strike={}
            strike['strike-Rate']=Number((Number(allYears[0][year]['runs'])/Number(allYears[0][year]['bowls']))*100)
            yearObj={}
            yearObj[year]=strike
            finalresult[player]=yearObj
        }
        
        
    }
    return finalresult
    
}

//code for extra problem5


function extraProblem5(matchesdata,deliveriesdata) {
    const bowlerData=[]
    const bowlers=deliveriesdata.reduce((bowler,index) =>{
        
       
        if(Number(index.inning)>2){
            let totalRunsInSuperOver=Number(index.noball_runs)+Number(index.wide_runs)+Number(index.batsman_runs)
            let superbowler=index.bowler
            if(superbowler in bowler){
                bowler[superbowler]['total_runs']=bowler[superbowler]['total_runs']+totalRunsInSuperOver
                if(Number(index.noball_runs==0) && Number(index.wide_runs)==0) {

                
                    bowler[superbowler]['total_over'] +=1
                }

            }
            else{
                bowlerData.push(superbowler)
                const runsInSuperOver={}
                runsInSuperOver['total_runs']=totalRunsInSuperOver
                if(Number(index.noball_runs==0) && Number(index.wide_runs)==0) {
                    runsInSuperOver['total_over']=0
                
                    
                }
                else {

                
                    runsInSuperOver['total_over']=1
                }
                bowler[superbowler]=runsInSuperOver
            }
        }
        return bowler
    
        
        
    },{})
    
    const besteconomy={}
    for(i=0;i<bowlerData.length;i++) 
    {
        
    
        let bowler=bowlerData[i]
        let totalOver=(Number(bowlers[bowler]['total_over'])/6)
        
        besteconomy[bowler]=Number(bowlers[bowler]['total_runs'])/totalOver

    }

    
    let minEconomicalBowler = Object.entries(besteconomy).sort((prev, next) => prev[1] - next[1])
    return minEconomicalBowler.shift()
    
    

}



//code for extra problem4

function extraProblem4(matchesdata,deliveriesdata) {
    const allPlayers=[]
    const bowlerAndPlayer=deliveriesdata.reduce((batsmanDismissedData,index) => {
        
        if(index.player_dismissed!="" && index.dismissal_kind!="run out") {

            if(index.player_dismissed in batsmanDismissedData) {
                let obj=batsmanDismissedData[index.player_dismissed]
                if(index.bowler in obj) {
                    batsmanDismissedData[index.player_dismissed][index.bowler] +=1
                }
                else {
                    bowlerDismissedPlayer={}
                    bowlerDismissedPlayer[index.bowler]=1
                    batsmanDismissedData[index.player_dismissed][index.bowler]=1
                }
                

            }
            else{
                allPlayers.push(index.player_dismissed)
                bowlerDismissedPlayer={}
                bowlerDismissedPlayer[index.bowler]=1
                batsmanDismissedData[index.player_dismissed]=bowlerDismissedPlayer
            }
        }
        return batsmanDismissedData
    },{})
    let lengthOfAllPlayersDismissal=Object.keys(bowlerAndPlayer).length
    let c=0
    let finalresult={}
    
    for(i=0;i<lengthOfAllPlayersDismissal;i++) {
        let batsman=allPlayers[i]
        let fetchNestedObj
        fetchNestedObj=bowlerAndPlayer[batsman]
        
        let minEconomicalBowler = Object.entries(fetchNestedObj).sort((prev, next) => prev[1] - next[1])
        finalresult[batsman]=minEconomicalBowler.pop()
        


    }
    
    let highestNumberOfTimes={}
    let playername;
    let m=0;
    for(i=0;i<lengthOfAllPlayersDismissal;i++) {
        let player=allPlayers[i]
        if(finalresult[player][1]>m) {
            m=finalresult[player][1]
            playername=player
            highestNumberOfTimes=finalresult[player]
        }


    }
    const highestNumberOfTime={}
    highestNumberOfTime[playername]=highestNumberOfTimes
    return highestNumberOfTime
    

}









module.exports={
    numberOfMatches,
    matchesWonByPerTeamPerYear,
    extraRunsByEachteam,
    topTenBowlers,
    extraProblem1,
    extraProblem2,
    extraProblem3,
    extraProblem4,
    extraProblem5


}